## Workaround for large json

Waiting for solution in node.

```bash
jq '.data | to_entries | map_values(.value + { slug: .key }) | del(.. | objects | .paper.cardkingdom) | del( .. | objects | .paper.tcgplayer.buylist)' AllPrices.json > prices.json
```
