const cron = require("node-cron");
const fs = require("fs");
const mtgjson = require("./mtgjson");

/**   
* * * * * *
| | | | | |
| | | | | day of week
| | | | month
| | | day of month
| | hour
| minute
second ( optional )
 */
// schedule tasks to be run on the server
cron.schedule("* * * * *", function () {
    console.log("running a task every minute");
});


/**
 * Get Sets Data
 */
mtgjson.getSets();
const sets = require('./ressources/allSets.json');
mtgjson.addElements(sets, 'sets');
/**
 * Get Cards Data
 */
mtgjson.getCards();
const cards = mtgjson.parseCards();
mtgjson.addElements(cards, 'cards');
/**
 * Get Prices Data
 */
mtgjson.getPrices();
mtgjson.parsePrices();
const prices = require('./ressources/prices.json');
mtgjson.addElements(prices, 'prices');