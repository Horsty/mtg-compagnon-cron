"use strict";
const fetch = require("node-fetch");
const dir = "ressources";
const cardsUrl = "https://mtgjson.com/api/v5/AllPrintings.json";
const pricesUrl = "https://mtgjson.com/api/v5/AllPrices.json";
const setsUrl = "https://mtgjson.com/api/v5/SetList.json";
const https = require("https");
const fs = require("fs");
const jq = require('node-jq')

const { MongoClient } = require("mongodb");
const url = "mongodb://localhost:27017/";

const getData = async (url, pathToSave) => {
  try {
    const response = await fetch(url);
    const json = await response.json();
    fs.writeFileSync(pathToSave, JSON.stringify(json.data, null, 2), "utf8");
  } catch (error) {
    console.log(error);
  }
};

const getCards = () => {
  getData(cardsUrl, `./src/${dir}/allCards.json`);
}

const getSets = () => {
  getData(setsUrl, `./src/${dir}/allSets.json`)
}

const getPrices = () => {
  const file = fs.createWriteStream(`./src/${dir}/allPrices.json`);
  https.get(pricesUrl, function (response) {
    response.pipe(file);
  });
}

const parseCards = () => {
  const jsonData = require(`./${dir}/allCards.json`);
  const printings = jsonData;
  let cards = [];
  for (const editionAcronym in printings) {
    cards = [...cards, ...printings[editionAcronym].cards];
  }
  fs.writeFileSync(`./src/${dir}/cards.json`, JSON.stringify(cards), "utf8");
  return cards;
}

const parsePrices = () => {
  const filter = '.data | to_entries | map_values(.value + { slug: .key }) | del(.. | objects | .paper.cardkingdom) | del( .. | objects | .paper.tcgplayer.buylist)';
  const pathToJson = `./src/${dir}/allPrices.json`;
  jq.run(filter, pathToJson).then((output) => {
    fs.writeFile(`./src/${dir}/price.json`, output);
  }).catch((err) => {
    console.error(err)
  });
}

const addElements = (elements, collectionName) => {
  MongoClient.connect(url, function (err, db) {
    if (err) throw err;
    var dbo = db.db("magic-tools");
    dbo.collection(collectionName).insertMany(elements, function (err, res) {
      if (err) throw err;
      console.log("Number of documents inserted: " + res.insertedCount);
      db.close();
    });
  });
}

module.exports = {
  getCards,
  getPrices,
  getSets,
  parsePrices,
  parseCards,
  addElements
};
